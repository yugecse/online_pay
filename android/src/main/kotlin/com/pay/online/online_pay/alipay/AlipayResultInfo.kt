package com.pay.online.online_pay.alipay

import android.text.TextUtils

class AlipayResultInfo(rawResult: Map<String, String>?) {
    /**
     * @return the resultStatus
     */
    var resultStatus: String? = null
        private set

    /**
     * @return the result
     */
    var result: String? = null
        private set

    /**
     * @return the memo
     */
    var memo: String? = null
        private set

    init {
        rawResult?.let {
            for (key in rawResult.keys) {
                when {
                    TextUtils.equals(key, "resultStatus") -> resultStatus = rawResult[key]
                    TextUtils.equals(key, "result") -> result = rawResult[key]
                    TextUtils.equals(key, "memo") -> memo = rawResult[key]
                }
            }
        }
    }

    override fun toString(): String = "resultStatus={$resultStatus};memo={$memo};result={$result}"
}