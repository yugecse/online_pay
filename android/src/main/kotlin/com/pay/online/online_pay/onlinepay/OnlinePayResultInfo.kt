package com.pay.online.online_pay.onlinepay

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class OnlinePayResultInfo(
    var onlinePayType: OnlinePayType? = null,
    var resultType: OnlinePayResultType? = null,
    var message: String? = null,
) : Parcelable {

    fun toMap() = mapOf(
        "pay_type" to onlinePayType?.value,
        "result_type" to resultType?.value,
        "message" to message
    )

}
