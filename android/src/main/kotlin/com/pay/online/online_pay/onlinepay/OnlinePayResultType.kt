package com.pay.online.online_pay.onlinepay

enum class OnlinePayResultType(val value: Int) {

    Success(1),

    Error(-1),

    Canceled(0)

}