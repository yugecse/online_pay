package com.pay.online.online_pay.onlinepay

enum class OnlinePayType(val value: String) {

    Alipay("ALIPAY"),

    WechatPay("WECHAT_PAY")

}

fun String.toOnlinePayType() = OnlinePayType.values().firstOrNull { it.value == this }
