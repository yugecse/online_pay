package com.pay.online.online_pay.wxpay

import org.json.JSONObject

/**
 * 微信支付参数
 * @param appId 应用ID
 * @param packageValue 包名，常量：Sign=WXPay
 * @param partnerId 商家ID
 * @param prepayId 订单准备ID
 * @param sign 签名字符串
 * @param nonceStr 唯一标识字符串
 * @param timeStamp 时间戳
 * @param signType 签名类型
 */
data class WechatPayParams(
    var appId: String? = null,
    var packageValue: String? = "Sign=WXPay",
    var partnerId: String? = null,
    var prepayId: String? = null,
    var sign: String? = null,
    var nonceStr: String? = null,
    var timeStamp: String? = null,
    var signType: String? = null
) {

    companion object {

        /**
         * 从JSON中获取微信支付参数
         * @param jsonObject 微信字符参数JSON对象
         */
        @JvmStatic
        fun fromJsonObject(jsonObject: JSONObject) = WechatPayParams(
            jsonObject.optString("appId"),
            jsonObject.optString("packageValue"),
            jsonObject.optString("partnerId"),
            jsonObject.optString("prepayId"),
            jsonObject.optString("sign"),
            jsonObject.optString("nonceStr"),
            jsonObject.optString("timeStamp"),
            jsonObject.optString("signType")
        )

    }

}