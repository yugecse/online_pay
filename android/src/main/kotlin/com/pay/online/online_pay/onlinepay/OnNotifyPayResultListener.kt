package com.pay.online.online_pay.onlinepay

fun interface OnNotifyPayResultListener {

    fun onNotifyPayResult(result: OnlinePayResultInfo)

}