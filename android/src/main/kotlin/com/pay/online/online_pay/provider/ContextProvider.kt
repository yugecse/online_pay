package com.pay.online.online_pay.provider

import android.annotation.SuppressLint
import android.content.ContentProvider
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.net.Uri

internal class ContextProvider : ContentProvider() {

    companion object {

        @SuppressLint("StaticFieldLeak")
        @JvmStatic
        private var mContext: Context? = null

        @JvmStatic
        fun getContextInstance(): Context? = mContext

    }

    override fun onCreate(): Boolean {
        mContext = context
        return false
    }

    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor? = null

    override fun getType(uri: Uri): String? = null

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int = 0

    override fun insert(uri: Uri, values: ContentValues?): Uri? = null

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int = 0
}