/// 支付类型
enum PayType {
  ///支付宝支付
  aliPay,

  /// 微信支付
  wechatPay
}
