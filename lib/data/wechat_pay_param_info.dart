/// 微信支付参数
class WechatPayParamInfo {
  /// 应用ID
  String? appId;

  /// 常量值：Sign=WXPay
  String? packageValue;

  /// 合作者ID
  String? partnerId;

  /// 预付订单ID
  String? prepayId;

  /// 签名字符串
  String? sign;

  /// 随机字符串
  String? nonceStr;

  /// 时间戳
  String? timeStamp;

  WechatPayParamInfo(
      {this.appId,
      this.packageValue,
      this.partnerId,
      this.prepayId,
      this.sign,
      this.nonceStr,
      this.timeStamp});

  factory WechatPayParamInfo.fromJson(Map<String, dynamic> json) =>
      WechatPayParamInfo()
        ..appId = json['appId'] as String?
        ..packageValue = json['packageValue'] as String?
        ..partnerId = json['partnerId'] as String?
        ..prepayId = json['prepayId'] as String?
        ..sign = json['sign'] as String?
        ..nonceStr = json['nonceStr'] as String?
        ..timeStamp = json['timeStamp'] as String?;

  Map<String, dynamic> toJson() => {
        'appId': appId,
        'packageValue': packageValue,
        'partnerId': partnerId,
        'prepayId': prepayId,
        'sign': sign,
        'nonceStr': nonceStr,
        'timeStamp': timeStamp
      };
}
