/// 支付结果状态
enum OnlinePayResultState {
  /// 支付成功
  success,

  /// 支付失败
  fail,

  /// 支付被取消
  cancel
}
