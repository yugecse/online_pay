import 'dart:convert';

import 'pay_type.dart';

///支付信息
class OnlinePayInfo {
  /// 支付类型
  PayType? type;

  /// 支付参数
  dynamic payArguments;

  OnlinePayInfo({required this.type, required this.payArguments});

  Map<String, dynamic> toJson() => {
        'type': type?.toString(),
        'payArguments':
            payArguments is String ? payArguments : jsonDecode(payArguments)
      };
}
