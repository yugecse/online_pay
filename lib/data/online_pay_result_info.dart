import 'online_pay_result_state.dart';
import 'pay_type.dart';

/// 在线支付结果信息
class OnlinePayResultInfo {
  /// 支付类型
  PayType? type;

  /// 支付状态
  OnlinePayResultState? state;

  /// 支付结果描述
  String? description;

  OnlinePayResultInfo({this.type, this.state, this.description});

  factory OnlinePayResultInfo.fromJson(Map<String, dynamic> json) =>
      OnlinePayResultInfo()
        ..type = json['pay_type'] == 'ALIPAY'
            ? PayType.aliPay
            : (json['pay_type'] == 'WECHAT_PAY' ? PayType.wechatPay : null)
        ..state = json['result_type'] == 1
            ? OnlinePayResultState.success
            : (json['result_type'] == -1
                ? OnlinePayResultState.fail
                : OnlinePayResultState.cancel)
        ..description = json['message'] as String?;

  Map<String, dynamic> toJson() => {
        'pay_type': type.toString(),
        'result_type': state.toString(),
        'message': description
      };
}
