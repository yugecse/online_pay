import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'data/online_pay_info.dart';
import 'data/online_pay_result_info.dart';
import 'online_pay_method_channel.dart';

abstract class OnlinePayPlatform extends PlatformInterface {
  /// Constructs a OnlinePayPlatform.
  OnlinePayPlatform() : super(token: _token);

  static final Object _token = Object();

  static OnlinePayPlatform _instance = OnlinePayMethodChannel();

  /// The default instance of [OnlinePayPlatform] to use.
  ///
  /// Defaults to [OnlinePayMethodChannel].
  static OnlinePayPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [OnlinePayPlatform] when
  /// they register themselves.
  static set instance(OnlinePayPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<void> initialize({String? wxAppId}) {
    throw UnimplementedError("initialize(String?) hasn't been implemented.");
  }

  Future<OnlinePayResultInfo> startPay(OnlinePayInfo? info) {
    throw UnimplementedError(
        "startPay(OnlinePayInfo?) hasn't been implemented.");
  }

  Future<bool> get isWechatInstalled =>
      throw UnimplementedError('isWechatInstalled hasn\'t been implemented.');

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
