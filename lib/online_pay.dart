import 'data/online_pay_info.dart';
import 'data/online_pay_result_info.dart';
import 'online_pay_platform_interface.dart';

/// 在线支付
class OnlinePay {
  factory OnlinePay() => OnlinePay._internal();

  OnlinePay._internal();

  /// 初始化支持
  Future<void> initialize({String? wxAppId}) {
    return OnlinePayPlatform.instance.initialize(wxAppId: wxAppId);
  }

  /// 发起支付
  Future<OnlinePayResultInfo?> startPay(OnlinePayInfo? info) {
    return OnlinePayPlatform.instance.startPay(info);
  }

  /// 获取微信是否安装
  Future<bool> get isWechatInstalled =>
      OnlinePayPlatform.instance.isWechatInstalled;

  /// 获取平台版本
  Future<String?> getPlatformVersion() {
    return OnlinePayPlatform.instance.getPlatformVersion();
  }
}
