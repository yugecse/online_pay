import 'package:flutter_test/flutter_test.dart';
import 'package:online_pay/data/online_pay_info.dart';
import 'package:online_pay/data/online_pay_result_info.dart';
import 'package:online_pay/online_pay.dart';
import 'package:online_pay/online_pay_method_channel.dart';
import 'package:online_pay/online_pay_platform_interface.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockOnlinePayPlatform
    with MockPlatformInterfaceMixin
    implements OnlinePayPlatform {
  @override
  Future<String?> getPlatformVersion() => Future.value('42');

  @override
  Future<OnlinePayResultInfo> startPay(OnlinePayInfo? info) {
    throw UnimplementedError();
  }

  @override
  Future<void> initialize({String? wxAppId}) {
    throw UnimplementedError();
  }

  @override
  Future<bool> get isWechatInstalled => throw UnimplementedError();
}

void main() {
  final OnlinePayPlatform initialPlatform = OnlinePayPlatform.instance;

  test('$OnlinePayMethodChannel is the default instance', () {
    expect(initialPlatform, isInstanceOf<OnlinePayMethodChannel>());
  });

  test('getPlatformVersion', () async {
    MockOnlinePayPlatform fakePlatform = MockOnlinePayPlatform();
    OnlinePayPlatform.instance = fakePlatform;
    expect(await OnlinePay().getPlatformVersion(), '42');
  });
}
